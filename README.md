## Meals

This simple React Native application is based on navigation between screens and redux store management.
A main layout shows a set of icons about food categories for which you can explore a list of typical dishes
and mark your favorites in a proper list.

This project is a fun playground exercise taken from this RN great [course](https://github.com/academind/react-native-practical-guide-code/tree/06-navigation)
